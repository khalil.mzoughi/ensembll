RESET_PSWD_REQUEST = {
    "subject": "[Ensembll] Reset password",
    "content": """Vous recevez ce mail car vous avez demandé la réinitialisation du mot de passe associé à votre compte Ensembll.
Veuillez suivre ce lien pour le changer:

{reset_link}

Cordialement,
L'équipe Ensembll"""
}