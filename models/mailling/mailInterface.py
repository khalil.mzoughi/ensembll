import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class Mailling:
    
    def __init__(self, host, pswd, port=587, smtp_server="smtp.gmail.com"):
        self.smtp_server = smtp_server
        self.host = host
        self.pswd = pswd
        self.port = port

        self.server = None

    def connect(self):
        try:
            print("Start connect")
            context = ssl.create_default_context()
            self.server = smtplib.SMTP(self.smtp_server, self.port)
            print("Server created")
            self.server.ehlo()
            self.server.starttls(context=context)
            print("TLS engaged")
            self.server.ehlo()
            self.server.login(self.host, self.pswd)
            print("Connected")
        except Exception as e:
            print(e)

    def disconnect(self):
        if self.server != None:
            self.server.quit()

    def buildmail(self, receiver, subject, content):
        message = MIMEMultipart()
        message["From"] = self.host
        message["To"] = receiver
        message["Subject"] = subject

        message.attach(MIMEText(content, "plain"))
        
        return message.as_string()
    
    def sendmail(self, dest, content):
        self.server.sendmail(self.host, dest, content)